# Party Parrot

Party parrot est une interface mobile de gestion/recommandation d'événements, développée via Ionic Framework 3.

Ce projet a été réalisé par l'hexanôme H4304, département Informatique, INSA Lyon (2017 - 2018).

**Un APK de la dernière version en date est fourni dans `dist/`, si vous ne souhaitez pas vous embêter avec les étapes ci-dessous.**

## Prérequis

Afin de pouvoir lancer l'application et la compiler sous Android, plusieurs dépendances sont nécessaires :
- Node.js (min: v8.9.0)
- Les librairies `ionic` et `cordova` installées en mode global via la commande : `
npm install -g cordova ionic`
- Le SDK Android à jour, inclu dans le `PATH`. Il est recommandé de suivre [ce lien](https://cordova.apache.org/docs/fr/latest/guide/platforms/android/).
- Dans le dossier du projet, lancer la commande : `npm install`

## Lancement et compilation

Placez-vous dans le dossier du projet, et tapez la commande : `ionic serve --lab`

Cela vous permettra d'obtenir un pseudo-émulateur, sous navigateur, qui reproduit le comportement de l'application sur mobile.

Pour compiler l'application pour Android (et donc avoir un APK), lancez la commande `ionic cordova build android`
