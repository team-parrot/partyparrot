import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { DatePickerModule } from 'ion-datepicker';
import { LOCALE_ID } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { Dialogs } from "@ionic-native/dialogs";
import { ImagePicker } from '@ionic-native/image-picker';

import { SettingsPage } from '../pages/settings/settings';
import { SchedulePage } from "../pages/schedule/schedule";
import { FriendsPage } from '../pages/friends/friends';
import { HomePage } from '../pages/home/home';
import { DetailsPage } from '../pages/details/details';
import { CreatePage } from '../pages/create/create';
import { TabsPage } from '../pages/tabs/tabs';
import { HistoryPage } from '../pages/history/history';
import { MapPage } from '../pages/map/map';
import { TastesPage } from '../pages/tastes/tastes';

import {CategoryFaker} from "../fakers/category.faker";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {UserFaker} from "../fakers/user.faker";
import {TasteFaker} from "../fakers/taste.faker";
import {EventFaker} from "../fakers/event.faker";
import {LocationFaker} from "../fakers/location.faker";
import {PriceFaker} from "../fakers/price.faker";
import {ConfigService} from "../services/config.service";
import {AppdataService} from "../services/appdata.service";
import {UserDetailsPageModule} from "../pages/user-details/user-details.module";
import {ComponentsModule} from "../components/components.module";

@NgModule({
  declarations: [
    MyApp,
    SettingsPage,
    SchedulePage,
    FriendsPage,
    HomePage,
    DetailsPage,
    CreatePage,
    TabsPage,
    HistoryPage,
    MapPage,
    TastesPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ComponentsModule,
    UserDetailsPageModule,
    DatePickerModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SettingsPage,
    SchedulePage,
    FriendsPage,
    HomePage,
    DetailsPage,
    CreatePage,
    TabsPage,
    HistoryPage,
    MapPage,
    TastesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ConfigService,
    CategoryFaker,
    AppdataService,
    EventFaker,
    LocationFaker,
    PriceFaker,
    TasteFaker,
    UserFaker,
    Geolocation,
	ImagePicker,
    Dialogs,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    { provide: LOCALE_ID, useValue: "fr-FR" }
  ]
})
export class AppModule {
}
