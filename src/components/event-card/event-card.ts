import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Event} from "../../entities/event";
import {DomSanitizer, SafeStyle} from "@angular/platform-browser";
import {User} from "../../entities/user";

/**
 * Generated class for the EventCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'event-card',
  templateUrl: 'event-card.html'
})
export class EventCardComponent {

  private rdmUser: User;

  @Input() event: Event;
  @Input() subEvent: boolean = false;

  @Output() open = new EventEmitter<Event>();
  @Output() hide = new EventEmitter<Event>();
  @Output() go = new EventEmitter<Event>();

  constructor(public sanitizer: DomSanitizer) {

  }

  ngOnInit() {
    if (this.event.attendees.length > 0) {
      let index = Math.floor(Math.random()*this.event.attendees.length);

      this.rdmUser = this.event.attendees[index];
    }
  }

  getPicture(url: string) : SafeStyle {
    return this.sanitizer.bypassSecurityTrustStyle('linear-gradient(to top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0.5) 20%,rgba(0,0,0,0) 66%), url(' + url + ')');
  }

  getClassFromName(name: String) {
    return name.trim().toLowerCase().replace(/\s/g, '_').replace(/[^\w]/g, '');
  }

  emitGo() {
    this.go.emit(this.event);
  }

  emitHide() {
    this.hide.emit(this.event);
  }

  emitOpen() {
    this.open.emit(this.event);
  }

}
