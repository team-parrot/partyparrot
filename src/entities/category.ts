export class Category {

  constructor(public name: String, public image: String, public icon: String, public color ?: String) {

  }

}
