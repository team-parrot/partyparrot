import {Location} from "./location";
import {User} from "./user";
import {Price} from "./price";
import {Category} from "./category";

export class Event {

  constructor(
    public name: String,
    public image: String,
    public category: Category,
    public startDate: Date,
    public endDate: Date,
    public prices: Array<Price> = [Price.FREE],
    public location: Location,
    public attendees: Array<User>,
    public description: String,
    public affluence: Number,
    public tags: Array<String>,
    public subEvents: Array<Event> = [],
    public visibilityPublic: boolean = true,
    public parentEvent: Event = undefined,

  ) {

  }

  isAttendable() : Boolean {
    return 0 === this.subEvents.length
  }

}
