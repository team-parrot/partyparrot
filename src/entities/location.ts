export class Location {

  constructor(public street: string,
              public zipCode: string,
              public city: string,
              public country: string,
              public latitude: number,
              public longitude: number
  ) {}

}
