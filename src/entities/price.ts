export class Price {

  public static FREE = new Price("Free", 0);

  constructor(public label: String, public value ?: Number) {

  }

  isFree() : Boolean {
    return 0 === this.value;
  }

  /**
   * Check if you can pay what you want.
   * @returns {Boolean}
   */
  isPWYW() : Boolean {
    return 'undefined' === typeof this.value;
  }

}
