import {Event} from "./event";

export class User {

  private cacheRdmTaste: String;

  constructor(
    public firstname: String,
    public lastname: String,
    public email: String,
    public avatar: String,
    public tastes: Array<String>,
    public events: Array<Event>,
    public availability: Array<any>,
    public friends: Array<User>
  ) {

  }

  randomTaste() {
    if ('undefined' === typeof this.cacheRdmTaste) {
      let index = Math.floor(Math.random()*this.tastes.length);

      this.cacheRdmTaste = this.tastes[index];
    }

    return this.cacheRdmTaste;
  }

}
