export class AbstractFaker {

  generate(): any {
    throw Error("Generate must be overridden by mockers.");
  }

  generateMultiple(amount: Number): Array<any> {
    let fakes: Array<Object> = [];

    for (let i=0; i<amount; i++) {
      fakes.push(this.generate());
    }

    return fakes;
  }

  static takeSomeElements<T>(array: Array<T>) : Array<T> {
    let index = Math.floor(Math.random() * array.length/2); // Using length/2 to avoid having to small arrays.
    let offset =  Math.floor( Math.random() * (array.length - index));

    return array.slice(index, index+offset);
  }

}
