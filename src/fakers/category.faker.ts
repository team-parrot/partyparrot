import faker from 'faker';

import {Category} from "../entities/category";
import {AbstractFaker} from "./abstract.faker";


export class CategoryFaker extends AbstractFaker{

  generate(): Category {

    const tags = [
      "Sports collectifs",
      "Sports individuels",
      "Musique classique",
      "Jazz",
      "Hip Hop",
      "Rock",
      "Cuisine",
      "Théâtre",
      "Théâtre d'improvisation",
      "Arts de rue",
      "Soirée",
    ];

    return new Category(faker.random.arrayElement(tags), faker.image.nightlife(), "ionitron");
    // return new Category(faker.commerce.product(), faker.image.nightlife(), "ionitron");
  }

}
