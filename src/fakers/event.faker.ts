import {AbstractFaker} from "./abstract.faker";
import {Event} from "../entities/event";
import faker from 'faker';
import {UserFaker} from "./user.faker";
import {LocationFaker} from "./location.faker";
import {PriceFaker} from "./price.faker";
import {Injectable} from "@angular/core";
import {User} from "../entities/user";
import {Category} from "../entities/category";
import {CategoryFaker} from "./category.faker";

const MAX_USERS_AMOUNT = 40;
const PRICES_AMOUNT = 2;
const MAX_SUBEVENTS_AMOUNT = 5;
const DESCRIPTION_WORD_COUNT = 20;
const TAGS_AMOUNT = 5;
const MAX_IMAGE_INDEX = 5;
const DATE_DAY_INTERVAL = 60;

@Injectable()
export class EventFaker extends AbstractFaker {

  private subEventMode: boolean = false;
  private subEventUsers: Array<User> = [];

  private storedUsers: Array<User> = [];
  private storedCategories: Array<Category> = [];

  constructor(
    private userFaker: UserFaker,
    private locationFaker: LocationFaker,
    private priceFaker: PriceFaker,
    private categoryFaker: CategoryFaker
    ) {
    super();
  }

  setStoredUsers(users: Array<User>): void {
    this.storedUsers = users;
  }

  setStoredCategories(categories: Array<Category>): void {
    this.storedCategories = categories;
  }

  generate(): Event {

    console.info('[EventFaker] Generating a new fake Event');

    let usersAmount: number,
        containsSubEvents: boolean,
        users: Array<User>,
        subEvents: Array<Event> = [];

    // TODO : Be sure that every user has been in one event (easy method: put everyone in the first call maybe)
    if (true === this.subEventMode) {
      console.info('[EventFaker] Actually, it\'s a sub-event.');
      let randomUsersSliceIndex = faker.random.number(~~(this.subEventUsers.length/2));

      console.info(`[EventFaker] storedUsers length: ${this.subEventUsers.length}`);
      console.info(`[EventFaker] randomUsersSliceIndex: ${randomUsersSliceIndex}`);

      usersAmount = faker.random.number(this.subEventUsers.length - randomUsersSliceIndex);
      users       = this.subEventUsers.slice(randomUsersSliceIndex, randomUsersSliceIndex+usersAmount);

      console.info(`[EventFaker] usersAmount: ${usersAmount}`);

      console.debug(users);

    } else {
      if (0 === this.storedUsers.length) {
        usersAmount = faker.random.number(MAX_USERS_AMOUNT);
        users = this.userFaker.generateMultiple(usersAmount);
      } else {
        users = AbstractFaker.takeSomeElements(this.storedUsers);
        usersAmount = users.length;
      }

      containsSubEvents   = faker.random.boolean();
      let subEventsAmount = (true === containsSubEvents) ? faker.random.number(MAX_SUBEVENTS_AMOUNT) : 0;

      this.subEventMode   = containsSubEvents;
      this.subEventUsers  = users;

      subEvents = this.generateMultiple(subEventsAmount);

      this.subEventMode = false;
    }

    let now = +(new Date()),
        before = new Date(now - Math.random()*DATE_DAY_INTERVAL*86400*1000),
        after = new Date(now + Math.random()*DATE_DAY_INTERVAL*86400*1000),
        startDate = faker.date.between(before, after),
        endDate = new Date(+after + Math.random()*DATE_DAY_INTERVAL*86400*1000);

    const tags = [
      "Sports collectifs",
      "Sports individuels",
      "Musique classique",
      "Jazz",
      "Hip Hop",
      "Rock",
      "Cuisine",
      "Théâtre",
      "Théâtre d'improvisation",
      "Arts de rue",
      "Soirée",
    ];

    let event = new Event(
      faker.commerce.productName(),
      faker.image.image()+'/'+faker.random.number(MAX_IMAGE_INDEX),
      // Fix: image coherence with lorempixel through the whole app

      // Category
      (0 === this.storedCategories.length) ?
        this.categoryFaker.generate() : faker.random.arrayElement(this.storedCategories),
      // End Category
      startDate,
      endDate,
      this.priceFaker.generateMultiple(PRICES_AMOUNT),
      this.locationFaker.generate(),
      users,
      faker.lorem.sentence(DESCRIPTION_WORD_COUNT),
      usersAmount,
      [faker.random.arrayElement(tags)],
      subEvents,
      faker.random.boolean()
    );

    for (let subEvent of subEvents) {
      subEvent.parentEvent = event;
    }

    for (let user of users) {
      user.events.push(event);
    }

    return event;
  }
}
