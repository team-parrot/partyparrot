import {AbstractFaker} from "./abstract.faker";
import {Location} from "../entities/location";
import faker from 'faker';

export class LocationFaker extends AbstractFaker {

  static getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  generate(): Location {
    return new Location(
      faker.address.streetAddress(),
      faker.address.zipCode(),
      faker.address.city(),
      faker.address.country(),
      LocationFaker.getRandomArbitrary(45.78, 45.785), // 45.782938, 4.8760843
      LocationFaker.getRandomArbitrary(4.87, 4.88)
    );
  }
}
