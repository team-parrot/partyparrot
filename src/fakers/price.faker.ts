import {AbstractFaker} from "./abstract.faker";
import {Price} from "../entities/price";
import faker from 'faker';

const DEFAULT_POSSIBLE_LABELS = [
  'Tarif VA',
  'Plein Tarif',
  'Tarif -26 ans',
  'Tarif Etudiant',
  'Tarif Yolo', // TODO : Remove this
];

const DEFAULT_MAX_VALUE = 30;

export class PriceFaker extends AbstractFaker{

  possibleLabels: Array<string>;
  maxValue: number;

  constructor() {
    super();

    this.possibleLabels = DEFAULT_POSSIBLE_LABELS;
    this.maxValue       = DEFAULT_MAX_VALUE;
  }

  generate(): Price {
    return new Price(
      faker.random.arrayElement(this.possibleLabels),
      faker.random.number(this.maxValue)
    );
  }
}
