import {AbstractFaker} from "./abstract.faker";
import faker from 'faker';

export class TasteFaker extends AbstractFaker{

  generate(): string {

    const tags = [
      "Sports collectifs",
      "Sports individuels",
      "Musique classique",
      "Jazz",
      "Hip Hop",
      "Rock",
      "Cuisine",
      "Théâtre",
      "Théâtre d'improvisation",
      "Arts de rue",
      "Soirée",
    ];

    return faker.random.arrayElement(tags);
    // return faker.company.catchPhrase();
  }
}
