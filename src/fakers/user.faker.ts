import {Injectable} from "@angular/core";
import faker from 'faker';

import {AbstractFaker} from "./abstract.faker";
import {User} from "../entities/user";
import {TasteFaker} from "./taste.faker";
import {Event} from "../entities/event";


@Injectable()
export class UserFaker extends AbstractFaker {

  private storedUsers: Array<User> = [];
  private storedEvents: Array<Event>  = [];
  private storedTastes: Array<string> = [];

  constructor(private tasteFaker: TasteFaker) {
    super();
  }

  setStoredUsers(users: Array<User>) {
    this.storedUsers = users;
  }

  setStoredEvents(events: Array<Event>) {
    this.storedEvents = events;
  }

  setStoredTastes(tastes: Array<string>) {
    this.storedTastes = tastes;
  }

  generate(): User {
    let friends: Array<User> = UserFaker.takeSomeElements(this.storedUsers);

    let user = new User(
      faker.name.firstName(),
      faker.name.lastName(),
      faker.internet.email(),
      faker.image.avatar(),
      this.tasteFaker.generateMultiple(faker.random.number(2) + 2),
      UserFaker.takeSomeElements(this.storedEvents), // Todo: generated Events with user constraint
      [], // TODO : Generate random calendar
      friends
    );

    for (let friend of friends) {
      friend.friends.push(user)
    }

    return user;
  }

}
