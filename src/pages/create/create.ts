import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from "../home/home";
import { ImagePicker } from '@ionic-native/image-picker';

import { Dialogs } from '@ionic-native/dialogs';

@Component({
  selector: 'page-create',
  templateUrl: 'create.html'
})
export class CreatePage {

  public today = new Date();
  // public evtDate;

  constructor(public navCtrl: NavController, private dialogs: Dialogs,
  			  public imagePicker: ImagePicker) {

  }

  eventCreatedEvent() {
    console.log("eventCreatedEvent()");

    return this.dialogs.alert(
      "Le processus de création complet n'est pas montré…",
      'Événement créé !',
      'Cool !'
    ).then(() => console.log('Dialog dismissed'))
      .catch(e => console.log('Error displaying dialog', e))
      .then(() => this.navCtrl.push(HomePage));
  }

  cancelEvent() {
    console.log("cancelEvent()");
    return this.navCtrl.push(HomePage);
  }

	getPicture(){
		var options = {
				maximumImagesCount : 1
		};
		this.imagePicker.getPictures(options).then((results) => {
		  for (var i = 0; i < results.length; i++) {
     		 console.log('Image URI: ' + results[i]);
		  }
		}, 
		(err) => {
			console.log(err);	
		 });
	}
}
