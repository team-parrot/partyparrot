import { Component } from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import {Event} from "../../entities/event";
import {User} from "../../entities/user";
import {AppdataService} from "../../services/appdata.service";

@Component({
  selector: 'page-details',
  templateUrl: 'details.html'
})
export class DetailsPage {

  public event: Event;
  public bag: any;
  public userInEvent: boolean;
  public loggedUser: User;
  public ratingShown: boolean = false;
  public rating: number = 2.5;
  private now: Date = new Date();

  constructor(public navCtrl: NavController,
              public sanitizer: DomSanitizer,
              public toastCtrl: ToastController,
              public params: NavParams,
              public appdataService: AppdataService) {
      console.log(this.params.get("evt"));
      this.event = this.params.get("evt");
      this.bag = this.params.get("bag");

      this.loggedUser = this.appdataService.loggedUser;
      this.userInEvent = -1 !== this.event.attendees.indexOf(this.loggedUser);
  }

	getPicture(url: string) : SafeStyle {
		return this.sanitizer.bypassSecurityTrustStyle('linear-gradient(to top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0.5) 20%,rgba(0,0,0,0) 66%), url(' + url + ')');
	}

  hide(className: string) {
    let toDelete = document.getElementsByClassName(className);
    console.error(toDelete);
    for (let i = 0; i < toDelete.length; ++i) {
      toDelete.item(i).parentNode.parentNode.removeChild(toDelete.item(i).parentNode);
    }
  }

  hideEvt() {
    this.bag.hiddenEvents.push(this.event);

    let toast = this.toastCtrl.create({
      message: "L'événément a été masqué.",
      duration: 5000,
    });

    toast.present();

    this.navCtrl.pop();
  }

  mainEvtGo() {
    this.appdataService.registerToEvent(this.event);
    this.userInEvent = true;

    let toast = this.toastCtrl.create({
      message: "L'événément a bien été ajouté au planning.",
      duration: 5000,
    });

    toast.present();

    this.navCtrl.pop();
  }

  toggleRating() {
    this.ratingShown = !this.ratingShown;
  }

  postRating() {
    let toast = this.toastCtrl.create({
      message: "Votre avis a été pris en compte.",
      showCloseButton: true,
      closeButtonText: "OK !",
      duration: 10000
    });

    toast.present();
  }
}
