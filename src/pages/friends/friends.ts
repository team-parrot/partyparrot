import { Component } from '@angular/core';
import {LoadingController, NavController} from 'ionic-angular';
import {AppdataService} from "../../services/appdata.service";
import {User} from "../../entities/user";

import _ from 'lodash';
import {UserDetailsPage} from "../user-details/user-details";

@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage {

  private friends: Array<User>;  // TODO : use it.
  private user: User;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public appdataService: AppdataService
  ) {
    this.friends = this.appdataService.loggedUser.friends;
    this.user = this.appdataService.loggedUser;

  }

  ionViewDidLoad() {
    let loader = this.loadingCtrl.create({
      content: "Chargement...",
      duration: 300
    });

    loader.present();
    // this.generateFriendList();
  }

  findFriend(event: any) {
    let search = (event.target.value) ? event.target.value.toLowerCase() : '';

    this.friends = _.filter(
      this.user.friends,
      (friend: User) => {
        return -1 !== friend.lastname.toLowerCase().indexOf(search) || -1 !== friend.firstname.toLowerCase().indexOf(search)
      }
    );

  }

  getDetails(friend: User) {
    this.navCtrl.push(UserDetailsPage, {user: friend});

  }

}
