import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { DetailsPage } from '../details/details';
import { CreatePage } from '../create/create';
import { AppdataService } from "../../services/appdata.service";
import { Event } from "../../entities/event";

/**
 * Generated class for the History page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  private userEvents: Array<Event>;
  private pastEvents: Array<Event>;
  private futureEvents: Array<Event>;

  constructor(public navCtrl: NavController,
              public sanitizer: DomSanitizer,
              public appdataService: AppdataService) {
    this.userEvents = this.appdataService.loggedUser.events;

    const now = new Date();
    this.pastEvents = this.userEvents.filter(e => +e.startDate < +now);
    this.futureEvents = this.userEvents.filter(e => +e.startDate >= +now);

    console.error(this.userEvents.map(e => e.startDate));
    console.error(this.pastEvents.map(e => e.startDate));
    console.error(this.futureEvents.map(e => e.startDate));
  }

  getPicture(url: string) : SafeStyle {
    return this.sanitizer.bypassSecurityTrustStyle('linear-gradient(to top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0.5) 20%,rgba(0,0,0,0) 66%), url(' + url + ')');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad History');
  }

  hide(className: string) {
    let toDelete = document.getElementsByClassName(className);
    console.error(toDelete);
    for (let i = 0; i < toDelete.length; ++i) {
      toDelete.item(i).parentNode.parentNode.removeChild(toDelete.item(i).parentNode);
    }
  }

  openCreateEvent() {
    console.log("openCreateEvent()");
    this.navCtrl.push(CreatePage);
  }

  openDetails(evt) {
    // console.log(evt);
    this.navCtrl.push(DetailsPage, {evt: evt});
  }

}
