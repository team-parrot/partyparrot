import { Component } from '@angular/core';
import {NavController, ToastController} from 'ionic-angular';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { DetailsPage } from '../details/details';
import { CreatePage } from '../create/create';
import { AppdataService } from '../../services/appdata.service'
import {Event} from "../../entities/event";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

// public Events = [{
// 		name : "Soirée raclette",
// 		startDate : "1510126371",
// 		image : "http://orig08.deviantart.net/33be/f/2008/245/f/b/party_parrot_by_calzephyr.jpg",
// 		location : "Chez Michel",
// 		affluence : 10,
//     description : "Une bonne soirée raclette"
// 	},
// 	{
// 		name : "Un Doua De Jazz",
// 		startDate : "1510120000",
// 		image : "https://stockagehelloassoprod.blob.core.windows.net/images/logos/un-doua-de-jazz-d873d19553d44bfa8371c34f1c5c0289.jpg",
// 		location : "Divers",
// 		affluence : 300,
//     description : "Meilleur festival étudiant de jazz",
// 		subEvents : [{
// 			name : "Ambrose Akinmusire",
// 			startDate : "1510848890",
// 			image : "https://stockagehelloassoprod.blob.core.windows.net/images/logos/un-doua-de-jazz-d873d19553d44bfa8371c34f1c5c0289.jpg",
// 			location : "Astrée",
// 			affluence : 250
// 		}]
// 	}];

  public events: Array<Event>;
  public hiddenEvents: Array<Event>;
  public pageBag = {
    hiddenEvents: []
  };

  constructor(public navCtrl: NavController,
              public toastCtrl: ToastController,
              public appDataService: AppdataService) {
    const now = new Date();
    this.events = this.appDataService.getFutureEvents().filter(e => +e.startDate >= +now);
    this.hiddenEvents = [];
  }

  letsGoTo(event: Event) {
    this.appDataService.registerToEvent(event);
    this.events = this.appDataService.getFutureEvents();
    this.hiddenEvents.push(event);

    let toast = this.toastCtrl.create({
      message: "L'événement a été ajouté dans votre planning.",
      showCloseButton: true,
      closeButtonText: "OK !",
      duration: 10000
    });

    toast.present();
  }

	openCreateEvent() {
		console.log("openCreateEvent()");
    this.navCtrl.push(CreatePage);
	}

	openDetails(evt) {
		// console.log(evt);
		this.navCtrl.push(DetailsPage, {evt: evt, bag: this.pageBag})
  }

	hide(event: Event) {
    let index = this.events.indexOf(event);
    this.events.splice(index, 1);
  }

  ionViewDidEnter() {
    this.events = this.appDataService.getFutureEvents();
    if (this.pageBag.hiddenEvents.length > 0) {
      this.hiddenEvents.push(...this.pageBag.hiddenEvents);

      this.pageBag.hiddenEvents = [];
    }
  }
}
