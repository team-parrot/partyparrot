// vim: tabstop=8 expandtab shiftwidth=2

import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {AppdataService} from "../../services/appdata.service";


declare var google: any;

@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  private map;
  public events;

  constructor(public navCtrl: NavController, public appDataService: AppdataService) {
    this.events = this.appDataService.events;
  }

  ionViewDidLoad() {
    let options = {
      center: new google.maps.LatLng(45.782938, 4.8760843), // INSA Lyon
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(document.getElementById("map_canvas"), options);
    this.populateMap();
    this.placeUser();
  }

  placeUser() {
    let locMarker = new google.maps.Marker({
      position: new google.maps.LatLng(45.78206, 4.8727),
      map: this.map,
      icon: {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 7,
        strokeColor:"#fefefe",
        fillColor:"#488aff",
        fillOpacity:1,
        strokeWeight: 2
      }
    });

  }

  mapDetail() {
    console.debug("COUCOU");
  }

  populateMap() {
    for (let event of this.events) {
      const contentString = '<div id="content">'+
        '<h1 id="firstHeading" class="firstHeading">' + event.name +'</h1>'+
        '<div id="bodyContent">'+
        '<p>'+
        event.description +
        '</p>'+
        '</div>'+
        '</div>';
      let infowindow = new google.maps.InfoWindow({
        content: contentString
      });

      let marker = new google.maps.Marker({
        position: new google.maps.LatLng(event.location.latitude, event.location.longitude),
        map: this.map,
        title: event.name
      });
      marker.addListener('click', function() {
        infowindow.open(this.map, marker);
      });
    }
  }
}
