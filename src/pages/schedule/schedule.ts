import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import _ from 'lodash';

/**
 * Generated class for the SchedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
})
export class SchedulePage {

  public range: Array<number>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.range = _.range(15);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SchedulePage');
  }

}
