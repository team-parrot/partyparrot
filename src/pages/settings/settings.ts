import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SchedulePage} from "../schedule/schedule";
import {TastesPage} from "../tastes/tastes";

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  constructor(public navCtrl: NavController) {

  }

  openSchedule(){
    this.navCtrl.push(SchedulePage);
  }

  openTastes(){
    this.navCtrl.push(TastesPage);
  }

}
