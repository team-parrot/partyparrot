import { Component } from '@angular/core';

import { SettingsPage } from '../settings/settings';
import { FriendsPage } from '../friends/friends';
import { MapPage } from '../map/map';
import { HomePage } from '../home/home';
import { HistoryPage } from '../history/history';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = MapPage;
  tab3Root = SettingsPage;
  tab4Root = FriendsPage;
  tab5Root = HistoryPage;

  constructor() {

  }
}
