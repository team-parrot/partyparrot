import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TastesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-tastes',
  templateUrl: 'tastes.html',
})
export class TastesPage {

  public userTags = [
    "Musique classique",
    "Théâtre d'improvisation"
  ];
  public tags = [
    "Sports collectifs",
    "Sports individuels",
    "Musique classique",
    "Jazz",
    "Hip Hop",
    "Rock",
    "Cuisine",
    "Théâtre",
    "Théâtre d'improvisation",
    "Arts de rue",
    "Soirée",
  ];

  public searchInput = "";


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TastesPage');
  }

  remove(userTag){
	this.userTags.splice(this.userTags.indexOf(userTag),1);
  }
  add(tag){
	if(this.userTags.indexOf(tag) == -1) this.userTags.push(tag);
  }
  passesSearch(tag){
	var passes = true;
	if(this.searchInput != "") {
		if(!tag.includes(this.searchInput) && !tag.toLowerCase().includes(this.searchInput)) {
			passes = false;
		}
	}
	console.log(tag+" passes "+passes);
	return passes;
  }
}
