import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {User} from "../../entities/user";
import {FriendsPage} from "../friends/friends";
import {ConfigService} from "../../services/config.service";
import {Event} from "../../entities/event";
import {DetailsPage} from "../details/details";

/**
 * Generated class for the UserDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-details',
  templateUrl: 'user-details.html',
})
export class UserDetailsPage {

  user: User;
  tastesPreview: Array<String>;

  currentSegment: string = 'informations';

  constructor(public navCtrl: NavController, public navParams: NavParams, private config: ConfigService) {
    this.user = navParams.get('user');

    this.tastesPreview = this.user.tastes.slice(0, this.config.pages.userDetails.tastePreviewAmount);
  }

  expandTastes(): void {
    this.tastesPreview = this.user.tastes;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserDetailsPage');
  }

  gotoEvent(event: Event) {
    this.navCtrl.push(DetailsPage, {evt: event});
  }

  gotoUserDetail(user: User) {
    this.navCtrl.push(UserDetailsPage, {user})
  }

}
