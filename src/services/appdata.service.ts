
import {Injectable} from "@angular/core";
import {CategoryFaker} from "../fakers/category.faker";
import {EventFaker} from "../fakers/event.faker";
import {UserFaker} from "../fakers/user.faker";
import {Event} from "../entities/event";
import {User} from "../entities/user";
import {Category} from "../entities/category";
import {ConfigService} from "./config.service";


@Injectable()
export class AppdataService {

  public categories: Array<Category> = [];
  public events: Array<Event> = [];
  public users: Array<User> = [];
  public loggedUser: User;

  constructor(
    private config: ConfigService,
    private categoryFaker: CategoryFaker,
    private eventFaker: EventFaker,
    private userFaker: UserFaker
  ) {

    // --- Categories generation
    this.categories = this.categoryFaker.generateMultiple(this.config.amount.categories);

    // --- Users Generation

    // Link our users array with the faker's one.
    this.userFaker.setStoredUsers(this.users);

    // Cannot use generateMultiple because of users array dependency
    for (let i = 0; i<this.config.amount.users; i++) {
      this.users.push(this.userFaker.generate());
    }

    // --- Event generation
    this.eventFaker.setStoredCategories(this.categories);
    this.eventFaker.setStoredUsers(this.users);
    this.events = this.eventFaker.generateMultiple(this.config.amount.events);

    let now = +new Date();

    this.events.sort((a, b) => {
      return Math.abs(now - (+a.startDate)) - Math.abs(now - (+b.startDate));
    });

    // --- Logged user generation (just a random user in the array)
    let rdmIndex = Math.floor(Math.random() * this.users.length);
    this.loggedUser = this.users[rdmIndex];

  }

  /**
   * For the logged user, get future events (does not include the ones in which the user is already registered).
   */
  getFutureEvents(): Array<Event> {
    return this.events.filter(
      event => -1 === event.attendees.indexOf(this.loggedUser) && +event.startDate > +new Date()
    );
  }

  registerToEvent(event: Event) {
    this.loggedUser.events.push(event);
    event.attendees.push(this.loggedUser);
  }

}
