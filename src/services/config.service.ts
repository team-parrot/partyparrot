export class ConfigService {

  public amount = {
    users: 30,
    categories: 12,
    events: 15
  }

  public pages = {
    userDetails: {
      tastePreviewAmount: 3
    }
  }

}
